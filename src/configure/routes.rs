use crate::handlers::graphql::{graphql, graphql_playground};
use crate::resolvers::root::create_schema;
use actix_web::web;

pub fn register_routes(config: &mut web::ServiceConfig) {
    config
        .app_data(web::Data::new(create_schema()))
        .service(graphql)
        .service(graphql_playground);
}
