use diesel_async::pooled_connection::bb8::Pool;
use diesel_async::pooled_connection::AsyncDieselConnectionManager;
use diesel_async::AsyncPgConnection;
use std::env;

pub async fn get_async_connection_pool() -> Pool<AsyncPgConnection> {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    println!("conn to: {}", &database_url);
    let manager = AsyncDieselConnectionManager::<AsyncPgConnection>::new(database_url);
    Pool::builder().build(manager).await.unwrap()
}
