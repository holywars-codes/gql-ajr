use diesel::internal::derives::multiconnection::chrono::{DateTime, Utc};
use juniper::graphql_object;
use uuid::Uuid;

use crate::helpers::repos::user_repo;
use crate::models::post_model::PostModel;
use crate::models::user_model::UserModel;
use crate::resolvers::root::Context;

#[graphql_object(context = Context)]
impl PostModel {
    fn id(&self) -> Uuid {
        self.id.clone()
    }

    fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }
    fn updated_at(&self) -> DateTime<Utc> {
        self.updated_at
    }
    fn title(&self) -> &str {
        &self.title
    }

    fn content(&self) -> &str {
        &self.content
    }

    async fn user(&self, context: &Context) -> UserModel {
        user_repo::find_user_by_id(&context.pool, &self.user_id).await
    }
}
