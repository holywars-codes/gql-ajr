use std::string::String;

use diesel::internal::derives::multiconnection::chrono::{DateTime, Utc};
use juniper::graphql_object;
use uuid::Uuid;

use crate::helpers::repos::post_repo;
use crate::models::post_model::PostModel;
use crate::models::user_model::UserModel;
use crate::resolvers::root::Context;

#[graphql_object(context = Context)]
impl UserModel {
    fn id(&self) -> &Uuid {
        &self.id
    }

    fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }
    fn updated_at(&self) -> DateTime<Utc> {
        self.updated_at
    }
    fn first_name(&self) -> &str {
        &self.first_name
    }

    fn last_name(&self) -> &str {
        &self.last_name
    }

    async fn posts(&self, context: &Context) -> Vec<PostModel> {
        // let conn = &mut context.pool.get().await.unwrap();
        post_repo::find_posts_by_user_id(&context.pool, &self.id).await
    }
}
