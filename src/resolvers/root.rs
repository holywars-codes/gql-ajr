use diesel_async::AsyncPgConnection;
use diesel_async::pooled_connection::bb8::Pool;
use juniper::{EmptySubscription, FieldResult, graphql_object, RootNode};
use uuid::Uuid;

use crate::helpers::repos::{post_repo, user_repo};
use crate::models::{
    post_model::{PostInput, PostModel},
    user_model::{UserInput, UserModel},
};

pub struct Context {
    pub pool: Pool<AsyncPgConnection>,
}

impl juniper::Context for Context {}
pub struct QueryRoot;

#[graphql_object(context = Context)]
impl QueryRoot {
    #[graphql(description = "List of all users")]
    async fn users(context: &Context) -> FieldResult<Vec<UserModel>> {
        Ok(user_repo::find_users(&context.pool).await)
    }

    #[graphql(description = "Get user by id")]
    async fn user(context: &Context, id: String) -> FieldResult<UserModel> {
        let uid = Uuid::parse_str(&id).unwrap();
        Ok(user_repo::find_user_by_id(&context.pool, &uid).await)
    }
    #[graphql(description = "List of all posts")]
    async fn posts(context: &Context) -> FieldResult<Vec<PostModel>> {
        Ok(post_repo::find_posts(&context.pool).await)
    }

    #[graphql(description = "Get post by id")]
    async fn post(context: &Context, id: String) -> FieldResult<PostModel> {
        let uid = Uuid::parse_str(&id).unwrap();
        Ok(post_repo::find_post_by_id(&context.pool, &uid).await)
    }
}

pub struct MutationRoot;

#[graphql_object(Context = Context)]
impl MutationRoot {
    // #[graphql(description = "Create post")]
    async fn create_post(context: &Context, post_input: PostInput) -> FieldResult<PostModel> {
        Ok(post_repo::create_post(&context.pool, post_input).await)
    }

    // #[graphql(description = "Create user")]
    async fn create_user(context: &Context, user_input: UserInput) -> FieldResult<UserModel> {
        Ok(user_repo::create_user(&context.pool, user_input).await)
    }
}

pub type SchemaApp = RootNode<'static, QueryRoot, MutationRoot, EmptySubscription<Context>>;

pub fn create_schema() -> SchemaApp {
    SchemaApp::new(QueryRoot {}, MutationRoot {}, EmptySubscription::new())
}

/*
https://github.com/actix/examples/blob/master/graphql/juniper/src/schema.rs
*/
