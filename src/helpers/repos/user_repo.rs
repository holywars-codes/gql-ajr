use diesel::query_dsl::methods::FilterDsl;
use diesel::query_dsl::select_dsl::SelectDsl;
use diesel::ExpressionMethods;
use diesel::{insert_into, SelectableHelper};
use diesel_async::pooled_connection::bb8::Pool;
use diesel_async::AsyncPgConnection;
use diesel_async::RunQueryDsl;
use uuid::Uuid;

use crate::models::user_model::{UserInput, UserModel};
use crate::schema::users;

pub async fn find_user_by_id(pool: &Pool<AsyncPgConnection>, user_id: &Uuid) -> UserModel {
    let conn = &mut pool.get().await.unwrap();

    users::dsl::users
        .filter(users::dsl::id.eq(&user_id))
        .select(UserModel::as_select())
        .get_result(conn)
        .await
        .unwrap()
}

pub async fn find_users(pool: &Pool<AsyncPgConnection>) -> Vec<UserModel> {
    let conn = &mut pool.get().await.unwrap();

    users::dsl::users
        .select(UserModel::as_select())
        .load(conn)
        .await
        .unwrap()
}

pub(crate) async fn create_user(
    pool: &Pool<AsyncPgConnection>,
    user_input: UserInput,
) -> UserModel {
    let conn = &mut pool.get().await.unwrap();

    insert_into(users::dsl::users)
        .values(user_input)
        .get_result(conn)
        .await
        .unwrap()
}
