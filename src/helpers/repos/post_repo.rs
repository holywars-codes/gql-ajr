use crate::models::post_model::{PostInput, PostModel};
use crate::schema::posts;
use diesel::ExpressionMethods;
use diesel::{insert_into, QueryDsl, SelectableHelper};
use diesel_async::pooled_connection::bb8::Pool;
use diesel_async::AsyncPgConnection;
use diesel_async::RunQueryDsl;
use uuid::Uuid;

pub async fn find_posts_by_user_id(
    pool: &Pool<AsyncPgConnection>,
    user_id: &Uuid,
) -> Vec<PostModel> {
    let conn = &mut pool.get().await.unwrap();

    posts::dsl::posts
        .select(PostModel::as_select())
        .filter(posts::dsl::user_id.eq(user_id))
        .load(conn)
        .await
        .unwrap()
}

pub async fn find_post_by_id(pool: &Pool<AsyncPgConnection>, post_id: &Uuid) -> PostModel {
    let conn = &mut pool.get().await.unwrap();

    posts::dsl::posts
        .select(PostModel::as_select())
        .filter(posts::dsl::id.eq(post_id))
        .get_result(conn)
        .await
        .unwrap()
}

pub(crate) async fn find_posts(pool: &Pool<AsyncPgConnection>) -> Vec<PostModel> {
    let conn = &mut pool.get().await.unwrap();

    posts::dsl::posts
        .select(PostModel::as_select())
        .load(conn)
        .await
        .unwrap()
}

pub(crate) async fn create_post(
    pool: &Pool<AsyncPgConnection>,
    post_input: PostInput,
) -> PostModel {
    let conn = &mut pool.get().await.unwrap();

    insert_into(posts::dsl::posts)
        .values(post_input)
        .get_result(conn)
        .await
        .unwrap()
}
