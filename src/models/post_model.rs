use diesel::internal::derives::multiconnection::chrono::{DateTime, Utc};
use diesel::prelude::*;
use juniper::GraphQLInputObject;
use serde::Deserialize;
use uuid::Uuid;

#[derive(Queryable, Selectable, Eq, PartialOrd, PartialEq)]
#[diesel(table_name = crate::schema::posts)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct PostModel {
    pub id: Uuid,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub title: String,
    pub content: String,
    pub user_id: Uuid,
}

#[derive(Insertable, Deserialize)]
#[diesel(table_name = crate::schema::posts)]
#[derive(GraphQLInputObject)]
#[graphql(description = "Post Input")]
pub struct PostInput {
    // pub id: Uuid,
    pub title: String,
    pub content: String,
    pub user_id: Uuid,
}
