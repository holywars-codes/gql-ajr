use diesel::internal::derives::multiconnection::chrono::{DateTime, Utc};
use diesel::prelude::*;
use juniper::GraphQLInputObject;
use serde::Deserialize;
use uuid::Uuid;

#[derive(Queryable, Selectable, Eq, PartialOrd, PartialEq)]
#[diesel(table_name = crate::schema::users)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct UserModel {
    pub id: Uuid,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub first_name: String,
    pub last_name: String,
}

#[derive(Insertable, Deserialize)]
#[diesel(table_name = crate::schema::users)]
#[derive(GraphQLInputObject)]
#[graphql(description = "User Input")]
pub struct UserInput {
    // pub id: Uuid,
    pub first_name: String,
    pub last_name: String,
}
