# Rust GraphQL service

## Configure ENV
``cp .env.example .env``
#### Edit file .env and save 

## Migrations
### Install diesel-cli and run
``diesel migration run``

## Dev
### Build:
``cargo build``
### Run:
``./target/debug/gql-ajr``

## Prod
### Build:
``cargo build --release``
### Run:
``./target/release/gql-ajr``

## Open playground in browser
[Click Here -> http://127.0.0.1:8080/graphiql](http://127.0.0.1:8080/graphiql)