create table public.posts
(
    id         uuid        default uuid_generate_v4() not null
        constraint posts_pk
            primary key,
    created_at timestamptz default current_timestamp  not null,
    updated_at timestamptz default current_timestamp  not null,
    title      varchar(64)                            not null,
    content    varchar(512)                           not null,
    user_id    uuid                                   not null
        constraint posts_users_id_fk
            references public.users
);

create index posts_user_id_index
    on public.posts (user_id);

