create table public.users
(
    id         uuid        default uuid_generate_v4() not null
        constraint users_pk
            primary key,
    created_at timestamptz default current_timestamp  not null,
    updated_at timestamptz default current_timestamp  not null,
    first_name varchar(32)                            not null,
    last_name  varchar(32)                            not null
);
